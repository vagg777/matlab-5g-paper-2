function [dl_data_rates,ul_data_rates] = associationAlgorithm(devices,device_dl_rb_demands,device_ul_rb_demands,device_dl_throughput_demands,device_ul_throughput_demands,rb_bandwidth,sinr_dl_macro_cells,sinr_dl_small_cells,sinr_ul_macro_cells,sinr_ul_small_cells,dl_resource_blocks,ul_resource_blocks,devices_dl_rb_demands_eNB_index,devices_ul_rb_demands_eNB_index,downlink_subcarriers,uplink_subcarriers,dl_data_rates,ul_data_rates,available_dl_RBs_macro_cells,available_dl_RBs_small_cells,available_ul_RBs_macro_cells,available_ul_RBs_small_cells,macro_cells,small_cells,macro_cells_x_pos,macro_cells_y_pos,devices_macro_dist,modulation_matrix,coding_rate_matrix,devices_small_dist)

% Calculate Downlink Resource Block demands for the UE-eNB association
[device_dl_rb_demands,devices_dl_rb_demands_eNB_index] = calculateDownlinkRBDemands(devices,device_dl_rb_demands,device_dl_throughput_demands,rb_bandwidth,sinr_dl_macro_cells,sinr_dl_small_cells,devices_dl_rb_demands_eNB_index);

% Calculate Uplink Resource Block demands for the UE-eNB association
[device_ul_rb_demands,devices_ul_rb_demands_eNB_index] = calculateUplinkRBDemands(devices,device_ul_rb_demands,device_ul_throughput_demands,rb_bandwidth,sinr_ul_macro_cells,sinr_ul_small_cells,devices_ul_rb_demands_eNB_index);

% Initialize upcoming matrices
temp_dl_rb_device_demands = device_dl_rb_demands;
temp_ul_rb_device_demands = device_ul_rb_demands;
temp_sinr_dl_macro_cells = sinr_dl_macro_cells;
temp_sinr_dl_small_cells = sinr_dl_small_cells;
temp_sinr_ul_macro_cells = sinr_ul_macro_cells;
temp_sinr_ul_small_cells = sinr_ul_small_cells;
temp_dl_MCS_data_rates = zeros(1,macro_cells);
temp_ul_MCS_data_rates = zeros(1,macro_cells);
temp_average_dl_CQI_data_rates = zeros(macro_cells,15);
temp_average_ul_CQI_data_rates = zeros(macro_cells,15);
dl_macro_area_CQI = zeros(1,macro_cells);
ul_macro_area_CQI = zeros(1,macro_cells);
dl_small_cell_connections = 0;
dl_macro_cell_connections = 0;
ul_small_cell_connections = 0;
ul_macro_cell_connections = 0;
dl_unsupported_UEs = 0;
ul_unsupported_UEs = 0;
dl_preserved_QoS = 0;
ul_preserved_QoS = 0;
dl_optimal_failures = strings([1,devices]);
ul_optimal_failures = strings([1,devices]);
in_range_devices = 0;



fprintf("\n----------------- Running association algorithm -----------------\n\n");



% [DOWNLINK] MCS Measurements
for i=1:macro_cells
    for CQI=1:15
        for j=1:devices
            if devices_macro_dist(j,i) < 400.0
                in_range_devices = in_range_devices + 1;
                temp_dl_MCS_data_rates(1,i) = temp_dl_MCS_data_rates(1,i) + (device_dl_rb_demands(1,j) * coding_rate_matrix(CQI,1) * rb_bandwidth * (1-10^(-6)))/10^6;
            end
        end
        if temp_dl_MCS_data_rates(1,i) ~= 0 % avoid NaN exception by dividing 0/0!
            temp_dl_MCS_data_rates(1,i) = temp_dl_MCS_data_rates(1,i)/in_range_devices;  % average downlink data rates per specific Macrocell
        end
        temp_average_dl_CQI_data_rates(i,CQI) = temp_dl_MCS_data_rates(1,i);               % average downlink data rates per CQI
        in_range_devices = 0;
    end
    
    [~,dl_CQI] = max(temp_average_dl_CQI_data_rates(i,:));   % optimal downlink average data rates(CQI) for specific Macrocell
    if (dl_CQI == 1)
        %fprintf(2,"[DOWNLINK] Macrocell area: %d | No devices in range!\n",i);
    else
        %fprintf("[DOWNLINK] Macrocell area: %d | Optimal MCS: %s | Optimal average CQI: %d | Optimal data rates %.2f \n",i,modulation_matrix(dl_CQI),dl_CQI,MCS_dl_data_rates);
        dl_macro_area_CQI(1,i) = dl_CQI;    % save optimal CQI for macrocell area for later use (CQI[1,3] = 5 means that macro area 3 has optimal CQI = 5)
    end
end



% [UPLINK] MCS Measurements
for i=1:macro_cells
    for CQI=1:15
        for j=1:devices
            if devices_macro_dist(j,i) < 400.0
                in_range_devices = in_range_devices + 1;
                temp_ul_MCS_data_rates(1,i) = temp_ul_MCS_data_rates(1,i) + (device_ul_rb_demands(1,j) * coding_rate_matrix(CQI,1) * rb_bandwidth * (1-10^(-4)))/10^6;
            end
        end
        if temp_ul_MCS_data_rates(1,i) ~= 0 % avoid NaN exception by dividing 0/0!
            temp_ul_MCS_data_rates(1,i) = temp_ul_MCS_data_rates(1,i)/in_range_devices;  % average uplink data rates per specific Macrocell
        end
        temp_average_ul_CQI_data_rates(i,CQI) = temp_ul_MCS_data_rates(1,i);               % average uplink data rates per CQI
        in_range_devices = 0;
    end
    
    [~,ul_CQI] = max(temp_average_ul_CQI_data_rates(i,:));   % optimal uplink average data rates(CQI) for specific Macrocell
    if (ul_CQI == 1)
        %fprintf(2,"[UPLINK] Macrocell area: %d | No devices in range!\n",i);
    else
        %fprintf("[UPLINK] Macrocell area: %d | Optimal MCS: %s | Optimal average CQI: %d | Optimal data rates %.2f \n",i,modulation_matrix(ul_CQI),ul_CQI,MCS_ul_data_rates);
        ul_macro_area_CQI(1,i) = ul_CQI;    % save optimal CQI for macrocell area for later use (CQI[1,3] = 5 means that macro area 3 has optimal CQI = 5)
    end
end




% [DOWNLINK] UE-BS association
for j=1:devices
    completed = false;
    inner_loops = 1;
    [demand, device_index] = min(temp_dl_rb_device_demands);
    eNB_index = devices_dl_rb_demands_eNB_index(device_index);
    [max_sinr, ~] = max([temp_sinr_dl_macro_cells(device_index,:),temp_sinr_dl_small_cells(device_index,:)]);
    while (completed==false)
        temp_dl_rb_device_demands(device_index) = 10^10;
        if (inner_loops >= 2)
            if (eNB_index <= macro_cells)
                temp_sinr_dl_macro_cells(device_index,eNB_index) = 0;
            else
                temp_sinr_dl_small_cells(device_index,eNB_index-macro_cells) = 0;
            end
            [max_sinr, new_eNB] = max([temp_sinr_dl_macro_cells(device_index,:),temp_sinr_dl_small_cells(device_index,:)]);
            device_dl_rb_demands(1,device_index) = ceil( (device_dl_throughput_demands(1,device_index))/(rb_bandwidth*log2(1+max_sinr)) );
            devices_dl_rb_demands_eNB_index(1,device_index) = new_eNB;
            if (new_eNB <= macro_cells)
                CQI = dl_macro_area_CQI(1,new_eNB);
            else
                small_cell = new_eNB-macro_cells;
                if (small_cell >= 1 && small_cell <=3)
                    CQI = dl_macro_area_CQI(1,1);           % macro area 1
                elseif (small_cell >= 4 && small_cell <=6)
                    CQI = dl_macro_area_CQI(1,2);           % macro area 2
                elseif (small_cell >= 7 && small_cell <=9)
                    CQI = dl_macro_area_CQI(1,4);          % macro area 4
                elseif (small_cell >= 10 && small_cell <=12)
                    CQI = dl_macro_area_CQI(1,5);          % macro area 5
                elseif (small_cell >= 13 && small_cell <=15)
                    CQI = dl_macro_area_CQI(1,3);          % macro area 3
                elseif (small_cell >= 16 && small_cell <=18)
                    CQI = dl_macro_area_CQI(1,6);          % macro area 6
                elseif (small_cell >= 19 && small_cell <=21)
                    CQI = dl_macro_area_CQI(1,7);          % macro area 7
                end
            end
            eNB_index = new_eNB;
        end
        if (eNB_index <= macro_cells)
            eNB_available_RBs = available_dl_RBs_macro_cells(eNB_index);
        else
            eNB_available_RBs = available_dl_RBs_small_cells(eNB_index-macro_cells);
        end
        if (eNB_available_RBs - demand) >= 0
            if (eNB_index <= macro_cells)
                if devices_macro_dist(device_index,eNB_index) < 450.0
                    if CQI ~= 0
                        dl_data_rates(1,device_index) = 12 * (device_dl_rb_demands(1,device_index) * ceil(coding_rate_matrix(CQI,1)) * rb_bandwidth * (1-10^(-4)))/10^6;
                    end
                    fprintf("[DOWNLINK] Device %d will connect with macro cell %d | data rates = %.2f(Mbps) | SINR = %.2f(dB)\n",device_index,eNB_index,dl_data_rates(1,device_index),10*log10(max_sinr));
                    available_dl_RBs_macro_cells(eNB_index) = available_dl_RBs_macro_cells(eNB_index) - demand;
                    dl_macro_cell_connections = dl_macro_cell_connections + 1;
                    dl_preserved_QoS = dl_preserved_QoS + 1;
                    dl_resource_blocks = dl_resource_blocks - demand;
                    downlink_subcarriers = downlink_subcarriers - demand*12;
                    completed = true;
                elseif (inner_loops == macro_cells+small_cells)
                    fprintf(2,"[DOWNLINK] Unable to serve device %d from the network !!!\n",device_index)
                    dl_unsupported_UEs = dl_unsupported_UEs + 1;
                    dl_data_rates(1,device_index) = 0;
                    completed = true;
                end
            else
                if devices_small_dist(device_index,eNB_index-macro_cells) < 80.0
                    if CQI ~= 0
                        dl_data_rates(1,device_index) = 12 * (device_dl_rb_demands(1,device_index) * ceil(coding_rate_matrix(CQI,1)) * rb_bandwidth * (1-10^(-4)))/10^6;
                    end
                    fprintf("[DOWNLINK] Device %d will connect with small cell %d | data rates = %.2f(Mbps) | SINR = %.2f (dB)\n",device_index,eNB_index-macro_cells,dl_data_rates(1,device_index),10*log10(max_sinr));
                    available_dl_RBs_small_cells(eNB_index-macro_cells) = available_dl_RBs_small_cells(eNB_index-macro_cells) - demand;
                    dl_small_cell_connections = dl_small_cell_connections + 1;
                    dl_preserved_QoS = dl_preserved_QoS + 1;
                    dl_resource_blocks = dl_resource_blocks - demand;
                    downlink_subcarriers = downlink_subcarriers - demand*12;
                    completed = true;
                elseif (inner_loops == macro_cells+small_cells)
                    fprintf(2,"[DOWNLINK] Unable to serve device %d from the network !!!\n",device_index)
                    dl_unsupported_UEs = dl_unsupported_UEs + 1;
                    dl_data_rates(1,device_index) = 0;
                    completed = true;
                end
            end
            inner_loops = inner_loops + 1;
        else
            dl_optimal_failures(1,device_index) = "yes";
            if (inner_loops == macro_cells+small_cells)
                fprintf(2,"[DOWNLINK] Unable to serve device %d from the network !!!\n",device_index)
                dl_unsupported_UEs = dl_unsupported_UEs + 1;
                dl_data_rates(1,device_index) = 0;
                completed = true;
            end
            inner_loops = inner_loops + 1;
        end
    end
end



fprintf("\n--------------------------------------------------------------------\n\n");




% [UPLINK] UE-BS association
for j=1:devices
    completed = false;
    inner_loops = 1;
    [demand, device_index] = min(temp_ul_rb_device_demands);
    eNB_index = devices_ul_rb_demands_eNB_index(device_index);
    [max_sinr, ~] = max([temp_sinr_ul_macro_cells(device_index,:),temp_sinr_ul_small_cells(device_index,:)]);
    while (completed==false)
        temp_ul_rb_device_demands(device_index) = 10^10;
        if (inner_loops >= 2)
            if (eNB_index <= macro_cells)
                temp_sinr_ul_macro_cells(device_index,eNB_index) = 0;
            else
                temp_sinr_ul_small_cells(device_index,eNB_index-macro_cells) = 0;
            end
            [max_sinr, new_eNB] = max([temp_sinr_ul_macro_cells(device_index,:),temp_sinr_ul_small_cells(device_index,:)]);
            device_ul_rb_demands(1,device_index) = ceil( (device_ul_throughput_demands(1,device_index))/(rb_bandwidth*log2(1+max_sinr)) );
            devices_ul_rb_demands_eNB_index(1,device_index) = new_eNB;
            if (new_eNB <= macro_cells)
                CQI = ul_macro_area_CQI(1,new_eNB);
            else
                small_cell = new_eNB-macro_cells;
                if (small_cell >= 1 && small_cell <=3)
                    CQI = ul_macro_area_CQI(1,1);           % macro area 1
                elseif (small_cell >= 4 && small_cell <=6)
                    CQI = ul_macro_area_CQI(1,2);           % macro area 2
                elseif (small_cell >= 7 && small_cell <=9)
                    CQI = ul_macro_area_CQI(1,4);           % macro area 4
                elseif (small_cell >= 10 && small_cell <=12)
                    CQI = ul_macro_area_CQI(1,5);           % macro area 5
                elseif (small_cell >= 13 && small_cell <=15)
                    CQI = ul_macro_area_CQI(1,3);           % macro area 3
                elseif (small_cell >= 16 && small_cell <=18)
                    CQI = ul_macro_area_CQI(1,6);           % macro area 6
                elseif (small_cell >= 19 && small_cell <=21)
                    CQI = ul_macro_area_CQI(1,7);           % macro area 7
                end
            end
            eNB_index = new_eNB;
        end
        if (eNB_index <= macro_cells)
            eNB_available_RBs = available_ul_RBs_macro_cells(eNB_index);
        else
            eNB_available_RBs = available_ul_RBs_small_cells(eNB_index-macro_cells);
        end
        if (eNB_available_RBs - demand) >= 0
            if (eNB_index <= macro_cells)
                if devices_macro_dist(device_index,eNB_index) < 450.0
                    if CQI ~= 0
                        ul_data_rates(1,device_index) = 12 * (device_ul_rb_demands(1,device_index) * coding_rate_matrix(CQI,1) * rb_bandwidth * (1-10^(-4)))/10^6;
                    end
                    fprintf("[UPLINK] Device %d will connect with macro cell %d | data rates = %.2f(Mbps) | SINR = %.2f(dB)\n",device_index,eNB_index,ul_data_rates(1,device_index),10*log10(max_sinr));
                    available_ul_RBs_macro_cells(eNB_index) = available_ul_RBs_macro_cells(eNB_index) - demand;
                    ul_macro_cell_connections = ul_macro_cell_connections + 1;
                    ul_preserved_QoS = ul_preserved_QoS + 1;
                    ul_resource_blocks = ul_resource_blocks - demand;
                    uplink_subcarriers = uplink_subcarriers - demand*12;
                    completed = true;
                elseif (inner_loops == macro_cells+small_cells)
                    fprintf(2,"[UPLINK] Unable to serve device %d from the network !!!\n",device_index)
                    ul_unsupported_UEs = ul_unsupported_UEs + 1;
                    ul_data_rates(1,device_index) = 0;
                    completed = true;
                end
            else
                if devices_small_dist(device_index,eNB_index-macro_cells) < 80.0
                    if CQI ~= 0
                        ul_data_rates(1,device_index) = 12 * (device_ul_rb_demands(1,device_index) * coding_rate_matrix(CQI,1) * rb_bandwidth * (1-10^(-4)))/10^6;
                    end
                    fprintf("[UPLINK] Device %d will connect with small cell %d | data rates = %.2f(Mbps) | SINR = %.2f (dB)\n",device_index,eNB_index-macro_cells,ul_data_rates(1,device_index),10*log10(max_sinr));
                    available_ul_RBs_small_cells(eNB_index-macro_cells) = available_ul_RBs_small_cells(eNB_index-macro_cells) - demand;
                    ul_small_cell_connections = ul_small_cell_connections + 1;
                    ul_preserved_QoS = ul_preserved_QoS + 1;
                    ul_resource_blocks = ul_resource_blocks - demand;
                    uplink_subcarriers = uplink_subcarriers - demand*12;
                    completed = true;
                elseif (inner_loops == macro_cells+small_cells)
                    fprintf(2,"[UPLINK] Unable to serve device %d from the network !!!\n",device_index)
                    ul_unsupported_UEs = ul_unsupported_UEs + 1;
                    ul_data_rates(1,device_index) = 0;
                    completed = true;
                end
            end
            inner_loops = inner_loops + 1;
        else
            ul_optimal_failures(1,device_index) = "yes";
            if (inner_loops == macro_cells+small_cells)
                fprintf(2,"[UPLINK] Unable to serve device %d from the network !!!\n",device_index)
                ul_unsupported_UEs = ul_unsupported_UEs + 1;
                ul_data_rates(1,device_index) = 0;
                completed = true;
            end
            inner_loops = inner_loops + 1;
        end
    end
end


fprintf("\n------------ UE-eNB Association results ------------\n\n");
fprintf("Network Devices : %d\n",devices)
fprintf("[DOWNLINK] Macro cell Connections :  %d\n",dl_macro_cell_connections);
fprintf("[DOWNLINK] Small cell Connections : %d\n",dl_small_cell_connections);
fprintf("[DOWNLINK] Unsupported devices : %d\n",dl_unsupported_UEs);
fprintf("[DOWNLINK] Successful Connections : %d\n",dl_macro_cell_connections+dl_small_cell_connections);
fprintf("[DOWNLINK] Successful Connection rate : %.2f %%\n",100*(dl_macro_cell_connections+dl_small_cell_connections)/(devices));
fprintf("[DOWNLINK] Devices that preserved QoS : %d\n",dl_preserved_QoS);
fprintf("[DOWNLINK] Preserved QoS rate: %.2f %%\n",100*(dl_preserved_QoS/(dl_macro_cell_connections+dl_small_cell_connections)));
fprintf("[DOWNLINK] Failures at completing optimal UE-eNB association : %d\n",sum(count(dl_optimal_failures,"yes")));
fprintf("[DOWNLINK] Optimal association failure rate : %.2f %%\n",100*(sum(count(dl_optimal_failures,"yes")))/(devices));
fprintf("[UPLINK] Macro cell Connections :  %d\n",ul_macro_cell_connections);
fprintf("[UPLINK] Small cell Connections : %d\n",ul_small_cell_connections);
fprintf("[UPLINK] Unsupported devices : %d\n",ul_unsupported_UEs);
fprintf("[UPLINK] Successful Connections : %d\n",ul_macro_cell_connections+ul_small_cell_connections);
fprintf("[UPLINK] Successful Connection rate : %.2f %%\n",100*(ul_macro_cell_connections+ul_small_cell_connections)/(devices));
fprintf("[UPLINK] Devices that preserved QoS : %d\n",ul_preserved_QoS);
fprintf("[UPLINK] Preserved QoS rate: %.2f %%\n",100*(ul_preserved_QoS/(ul_macro_cell_connections+ul_small_cell_connections)));
fprintf("[UPLINK] Failures at completing optimal UE-eNB association : %d\n",sum(count(ul_optimal_failures,"yes")));
fprintf("[UPLINK] Optimal association failure rate : %.2f %%\n",100*(sum(count(ul_optimal_failures,"yes")))/(devices));

end