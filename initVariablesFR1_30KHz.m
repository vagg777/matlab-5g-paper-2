function [devices,macro_cells,small_cells,rb_bandwidth,macro_cell_radiation,small_cell_radiation,ue_radiation,subcarrier_spacing,white_noise,carrier_frequency,dl_bandwidth,ul_bandwidth,devices_macro_dist,devices_small_dist,path_loss_macro_cells,path_loss_small_cells,channel_gain_macro_cells,channel_gain_small_cells,sinr_macro_cells,sinr_small_cells,device_dl_throughput_demands,device_ul_throughput_demands,downlink_subcarriers,uplink_subcarriers,dl_resource_blocks,ul_resource_blocks,device_dl_rb_demands,device_ul_rb_demands,devices_dl_rb_demands_eNB_index,devices_ul_rb_demands_eNB_index,dl_data_rates,ul_data_rates,available_dl_RBs_macro_cells,available_dl_RBs_small_cells,available_ul_RBs_macro_cells,available_ul_RBs_small_cells,modulation_matrix,coding_rate_matrix] = initVariablesFR1_30KHz()
    subcarrier_spacing = 30*10^3;                               % Subcarrier Spacing : 30 KHz (default)    
    dl_resource_blocks = 273;                                   % Downlink Resource Blocks = 273 (default)
    ul_resource_blocks = 273;                                   % Uplink Resource Blocks = 273 (default)
    rb_bandwidth = 360*10^3;                                    % RB Bandwidth : 360 KHz (default)
    devices = 400;                                              % Devices in network = 50 (default)
    carrier_frequency = (3.5)*10^9;                             % Carrier frequency : 3.5 GHz (default)
    dl_bandwidth = 100*10^6;                                    % Downlink Bandwidth : 100 MHz (default)
    ul_bandwidth = 100*10^6;                                    % Uplink Bandwidth : 100 MHz (default)
    macro_cells = 19;                                           % Macro cells = 19 (default)
    small_cells = 21;                                           % Small cells = 21 (default)
    macro_cell_radiation = 10^(46/10)/1000;                     % Convert MC radiation from 46 dBm to Watts (default) 
    small_cell_radiation = 10^(30/10)/1000;                     % Convert SC radiation from 30 dBm to Watts (default)
    ue_radiation = 10^(23/10)/1000;                             % Convert UE radiation from 23 dBm to Watts (default)
    white_noise = 10^(-174/10)/1000;                            % Convert white noise power density from -174dBm/Hz to Watts/Hz (default)
    downlink_subcarriers = dl_resource_blocks*12;               % Calculate subcarriers for the Downlink network = 3276 (default)
    uplink_subcarriers = ul_resource_blocks*12;                 % Calculate subcarriers for the Uplink network = 3276 (default)
    devices_macro_dist = zeros(devices,macro_cells);            % Distance of UE from each macro cell
    devices_small_dist = zeros(devices,small_cells);            % Distance of UE from each small cell
    path_loss_macro_cells = zeros(devices,macro_cells);         % Path Loss of UE from each macro cell
    path_loss_small_cells = zeros(devices,small_cells);         % Path Loss of UE from each small cell
    channel_gain_macro_cells = zeros(devices,macro_cells);      % Channel Gain of UE from each macro cell
    channel_gain_small_cells = zeros(devices,small_cells);      % Channel Gain of UE from each small cell
    sinr_macro_cells = zeros(devices,macro_cells);              % SINR of UE from each macro cell
    sinr_small_cells = zeros(devices,small_cells);              % SINR of UE from each small cell
    device_dl_throughput_demands = zeros(1,devices);            % Downlink Throughput demands of each UE
    device_ul_throughput_demands = zeros(1,devices);            % Uplink Throughput demands of each UE
    device_dl_rb_demands = zeros(1,devices);                    % Downlink Resource Block Demand value of each UE
    device_ul_rb_demands = zeros(1,devices);                    % Uplink Resource Block Demand value of each UE
    devices_dl_rb_demands_eNB_index = zeros(1,devices);         % Downlink eNB index of the RB demands of each UE
    devices_ul_rb_demands_eNB_index = zeros(1,devices);         % Uplink eNB index of the RB demands of each UE
    dl_data_rates = zeros(1,devices);                           % Downlink Data rates for each UE
    ul_data_rates = zeros(1,devices);                           % Uplink Data rates for each UE
    available_dl_RBs_macro_cells(1,1:macro_cells) = 3*ceil(dl_resource_blocks/macro_cells);   
    available_dl_RBs_small_cells(1,1:small_cells) = 3*ceil(dl_resource_blocks/small_cells);  
    available_ul_RBs_macro_cells(1,1:macro_cells) = 3*ceil(ul_resource_blocks/macro_cells);   
    available_ul_RBs_small_cells(1,1:small_cells) = 3*ceil(ul_resource_blocks/small_cells);  
    modulation_matrix = ["QPSK","QPSK","QPSK","QPSK","QPSK","QPSK","16QAM","16QAM","16QAM","64QAM","64QAM","64QAM","64QAM","64QAM","64QAM"]';
    coding_rate_matrix = [0.0762, 0.1172, 0.1885, 0.3008, 0.4385, 0.5879, 0.3691, 0.4785, 0.6016, 0.4551, 0.5537, 0.6504, 0.7539, 0.8525, 0.9258]';
end

