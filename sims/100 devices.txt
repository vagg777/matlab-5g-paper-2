FR1 - 30 KHz

------------ UE-eNB Association results ------------

Network Devices : 100
[DOWNLINK] Macro cell Connections :  99
[DOWNLINK] Small cell Connections : 1
[DOWNLINK] Unsupported devices : 0
[DOWNLINK] Successful Connections : 100
[DOWNLINK] Successful Connection rate : 100.00 %
[DOWNLINK] Devices that preserved QoS : 100
[DOWNLINK] Preserved QoS rate: 100.00 %
[DOWNLINK] Failures at completing optimal UE-eNB association : 3
[DOWNLINK] Optimal association failure rate : 3.00 %
[UPLINK] Macro cell Connections :  94
[UPLINK] Small cell Connections : 6
[UPLINK] Unsupported devices : 0
[UPLINK] Successful Connections : 100
[UPLINK] Successful Connection rate : 100.00 %
[UPLINK] Devices that preserved QoS : 100
[UPLINK] Preserved QoS rate: 100.00 %
[UPLINK] Failures at completing optimal UE-eNB association : 0
[UPLINK] Optimal association failure rate : 0.00 %

------------ Device Data Rate Results ------------

[DOWNLINK] Average data rate : 12.48 (Mbps)
[UPLINK] Average data rate : 8.32 (Mbps)

------------ SINR Results ------------

[DOWNLINK] Average macro cell SINR : 19.21
[DOWNLINK] Average small cell SINR : -2.47
[UPLINK] Average macro cell SINR : 16.51
[UPLINK] Average small cell SINR : 13.43

------------------------------------------------------------------------------------------------------

FRI - 60 KHz:

Network Devices : 100
[DOWNLINK] Macro cell Connections :  97
[DOWNLINK] Small cell Connections : 2
[DOWNLINK] Unsupported devices : 1
[DOWNLINK] Successful Connections : 99
[DOWNLINK] Successful Connection rate : 99.00 %
[DOWNLINK] Devices that preserved QoS : 99
[DOWNLINK] Preserved QoS rate: 100.00 %
[DOWNLINK] Failures at completing optimal UE-eNB association : 4
[DOWNLINK] Optimal association failure rate : 4.00 %
[UPLINK] Macro cell Connections :  94
[UPLINK] Small cell Connections : 6
[UPLINK] Unsupported devices : 0
[UPLINK] Successful Connections : 100
[UPLINK] Successful Connection rate : 100.00 %
[UPLINK] Devices that preserved QoS : 100
[UPLINK] Preserved QoS rate: 100.00 %
[UPLINK] Failures at completing optimal UE-eNB association : 0
[UPLINK] Optimal association failure rate : 0.00 %

------------ Device Data Rate Results ------------

[DOWNLINK] Average data rate : 13.35 (Mbps)
[UPLINK] Average data rate : 10.00 (Mbps)

------------ SINR Results ------------

[DOWNLINK] Average macro cell SINR : 19.21
[DOWNLINK] Average small cell SINR : -2.47
[UPLINK] Average macro cell SINR : 16.51
[UPLINK] Average small cell SINR : 13.43

------------------------------------------------------------------------------------------------------

FR2 - 60 KHz

------------ UE-eNB Association results ------------

Network Devices : 100
[DOWNLINK] Macro cell Connections :  99
[DOWNLINK] Small cell Connections : 1
[DOWNLINK] Unsupported devices : 0
[DOWNLINK] Successful Connections : 100
[DOWNLINK] Successful Connection rate : 100.00 %
[DOWNLINK] Devices that preserved QoS : 100
[DOWNLINK] Preserved QoS rate: 100.00 %
[DOWNLINK] Failures at completing optimal UE-eNB association : 0
[DOWNLINK] Optimal association failure rate : 0.00 %
[UPLINK] Macro cell Connections :  94
[UPLINK] Small cell Connections : 6
[UPLINK] Unsupported devices : 0
[UPLINK] Successful Connections : 100
[UPLINK] Successful Connection rate : 100.00 %
[UPLINK] Devices that preserved QoS : 100
[UPLINK] Preserved QoS rate: 100.00 %
[UPLINK] Failures at completing optimal UE-eNB association : 0
[UPLINK] Optimal association failure rate : 0.00 %

------------ Device Data Rate Results ------------

[DOWNLINK] Average data rate : 13.82 (Mbps)
[UPLINK] Average data rate : 9.68 (Mbps)

------------ SINR Results ------------

[DOWNLINK] Average macro cell SINR : 19.21
[DOWNLINK] Average small cell SINR : -2.47
[UPLINK] Average macro cell SINR : 16.51
[UPLINK] Average small cell SINR : 13.43

------------------------------------------------------------------------------------------------------

FR2 - 120 KHz

------------ UE-eNB Association results ------------

Network Devices : 100
[DOWNLINK] Macro cell Connections :  99
[DOWNLINK] Small cell Connections : 1
[DOWNLINK] Unsupported devices : 0
[DOWNLINK] Successful Connections : 100
[DOWNLINK] Successful Connection rate : 100.00 %
[DOWNLINK] Devices that preserved QoS : 100
[DOWNLINK] Preserved QoS rate: 100.00 %
[DOWNLINK] Failures at completing optimal UE-eNB association : 1
[DOWNLINK] Optimal association failure rate : 1.00 %
[UPLINK] Macro cell Connections :  94
[UPLINK] Small cell Connections : 6
[UPLINK] Unsupported devices : 0
[UPLINK] Successful Connections : 100
[UPLINK] Successful Connection rate : 100.00 %
[UPLINK] Devices that preserved QoS : 100
[UPLINK] Preserved QoS rate: 100.00 %
[UPLINK] Failures at completing optimal UE-eNB association : 0
[UPLINK] Optimal association failure rate : 0.00 %

------------ Device Data Rate Results ------------

[DOWNLINK] Average data rate : 18.78 (Mbps)
[UPLINK] Average data rate : 17.00 (Mbps)

------------ SINR Results ------------

[DOWNLINK] Average macro cell SINR : 19.21
[DOWNLINK] Average small cell SINR : -2.47
[UPLINK] Average macro cell SINR : 16.51
[UPLINK] Average small cell SINR : 13.43