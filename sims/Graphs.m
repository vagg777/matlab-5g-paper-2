function Graphs()

grid;
close all;
clc;

devices = [100 200 300 400];

% Graph 1 ([DL/UL] FR1 Device Data Rates)
dl_data_rates_FR1_30KHz = [12.48 13.06 12.13 13.99];
dl_data_rates_FR1_60KHz = [13.35 14.23 13.53 14.51];
ul_data_rates_FR1_30KHz = [8.32 7.98 7.55 7.45];
ul_data_rates_FR1_60KHz = [10.00 9.32 9.24 9.26];

% Graph 2 ([DL/UL] FR2 Device Data Rates)
dl_data_rates_FR2_60KHz = [13.82 14.33 13.53 14.52];
dl_data_rates_FR2_120KHz = [18.78 20.43 20.32 22.02];
ul_data_rates_FR2_60KHz = [9.68  9.68 9.21 9.24];
ul_data_rates_FR2_120KHz = [17.00 16.25 16.58 16.65];

% Graph 3 ([DL] FR1 Macro-Small connections 30/60 KHz)
dl_macro_connections_FR1_30KHz = 100*[99/100 167/178 196/224 245/274];
dl_small_connections_FR1_30KHz = 100*[1/100 11/178 28/224 29/274];
dl_macro_connections_FR1_60KHz = 100*[97/99 159/170 200/228 230/263];
dl_small_connections_FR1_60KHz = 100*[2/99 11/170 28/228 33/263];

% Graph 4 ([UL] FR1 Macro-Small connections 30/60 KHz)
ul_macro_connections_FR1_30KHz = 100*[94/100 176/200 254/288 312/353];
ul_small_connections_FR1_30KHz = 100*[6/100 24/200 34/288 41/353];
ul_macro_connections_FR1_60KHz = 100*[94/100 169/193 204/238 226/267];
ul_small_connections_FR1_60KHz = 100*[6/100 24/193 34/238 41/267];

% Graph 5 ([DL] FR2 Macro-Small connections 60/120 KHz)
dl_macro_connections_FR2_60KHz = 100*[99/100 191/199 243/265 300/329];
dl_small_connections_FR2_60KHz = 100*[1/100 8/199 22/265 29/329];
dl_macro_connections_FR2_120KHz = 100*[99/100 171/181 189/216 214/248];
dl_small_connections_FR2_120KHz = 100*[1/100 10/181 27/216 34/248];

% Graph 6 ([UL] FR2 Macro-Small connections 60/120 KHz)
ul_macro_connections_FR2_60KHz = 100*[94/100 176/200 264/298 326/367];
ul_small_connections_FR2_60KHz = 100*[6/100 24/200 34/298 41/367];
ul_macro_connections_FR2_120KHz = 100*[99/100 164/188 186/220 209/250];
ul_small_connections_FR2_120KHz = 100*[1/100 24/188 34/220 41/250];

% Graph 7 (FR1 Percentage of overall connections) ------- !!!! ---------
dl_connections_percent_FR1_30KHz = [100.00 89.00 74.67 68.50];
dl_connections_percent_FR1_60KHz = [99.00 85.00 76.00 65.75];
ul_connections_percent_FR1_30KHz = [100.00 100.00 96.00 88.25];
ul_connections_percent_FR1_60KHz = [100.00 96.50 79.33 66.75];

% Graph 8 (FR2 Percentage of overall connections) ------- !!!! ---------
dl_connections_percent_FR2_60KHz = [100.00 99.50 88.33 82.25];
dl_connections_percent_FR2_120KHz = [100.00 90.50 72.00 62.00];
ul_connections_percent_FR2_60KHz = [100.00 100.00 99.33 91.75];
ul_connections_percent_FR2_120KHz = [100.00 94.00 73.33 62.50];


%%%%%%%%%%% 1) FR1/FR2 Connection Percentage [DL] %%%%%%%%%%%%
% hold on;
% xlabel('Number of users') 
% ylabel('DL Macro/Small Connections in FR1/FR2 (%)')
% plot(devices, dl_macro_connections_FR1_30KHz,'-s');
% plot(devices, dl_macro_connections_FR2_60KHz,'-s');
% plot(devices, dl_small_connections_FR1_30KHz,'-o');
% plot(devices, dl_small_connections_FR2_60KHz,'-o');
% legend({'Macrocell connections (FR1 - 30 KHz SCS)','Macrocell connections (FR2 - 60 KHz SCS)','Small cell connections (FR1 - 30 KHz SCS)','Small cell connections (FR2 - 60 KHz SCS)'},'Location','southwest')
% hold off;

% %%%%%%%%%%% 2) FR1/FR2 Connection Percentage [UL] %%%%%%%%%%%%
% hold on;
% xlabel('Number of users') 
% ylabel('UL Macro/Small Connections in FR1/FR2 (%)')
% plot(devices, ul_macro_connections_FR1_30KHz,'-s');
% plot(devices, ul_macro_connections_FR2_60KHz,'-s');
% plot(devices, ul_small_connections_FR1_30KHz,'-o');
% plot(devices, ul_small_connections_FR2_60KHz,'-o');
% legend({'Macrocell connections (FR1 - 30 KHz SCS)','Macrocell connections (FR2 - 60 KHz SCS)','Small cell connections (FR1 - 30 KHz SCS)','Small cell connections (FR2 - 60 KHz SCS)'},'Location','southwest')
% hold off;

%%%%%%%%%%% 3) FR1/FR2 Device Data Rates [DL/UL] %%%%%%%%%%%%
% hold on;
% xlabel('Number of users') 
% ylabel('Average User Data Rates in FR1/FR2 (Mbps)')
% plot(devices, dl_data_rates_FR1_30KHz,'-s');
% plot(devices, dl_data_rates_FR2_60KHz,'-s');
% plot(devices, ul_data_rates_FR1_30KHz,'-o');
% plot(devices, ul_data_rates_FR2_60KHz,'-o');
% legend({'[DL] 30 KHz SCS (FR1)','[DL] 60 KHz SCS (FR2)','[UL] 30 KHz SCS (FR1)','[UL] 60 KHz SCS (FR2)'},'Location','southwest')
% hold off;

%%%%%%%%%%% 3) FR1/FR2 Device Data Rates [DL] %%%%%%%%%%%%
% hold on;
% xlabel('Number of users') 
% ylabel('Average DL User Data Rates in FR1/FR2 (Mbps)')
% axes = categorical({'100 (FR1)','200 (FR1)','300 (FR1)','400 (FR1)','100 (FR2)','200 (FR2)','300 (FR2)','400 (FR2)'});
% rates = [dl_data_rates_FR1_30KHz,dl_data_rates_FR2_60KHz];
% graph = bar(axes,rates);
% graph.FaceColor = 'flat';
% graph.CData(2,:) = [0.8500 0.3250 0.0980];
% graph.CData(4,:) = [0.8500 0.3250 0.0980];
% graph.CData(6,:) = [0.8500 0.3250 0.0980];
% graph.CData(8,:) = [0.8500 0.3250 0.0980];
% hold off;

%%%%%%%%%%% 3) FR1/FR2 Device Data Rates [UL] %%%%%%%%%%%%
% hold on;
% xlabel('Number of users') 
% ylabel('Average UL User Data Rates in FR1/FR2 (Mbps)')
% axes = categorical({'100 (FR1)','200 (FR1)','300 (FR1)','400 (FR1)','100 (FR2)','200 (FR2)','300 (FR2)','400 (FR2)'});
% rates = [ul_data_rates_FR1_30KHz,ul_data_rates_FR2_60KHz];
% graph = bar(axes,rates);
% graph.FaceColor = 'flat';
% graph.CData(2,:) = [0.8500 0.3250 0.0980];
% graph.CData(4,:) = [0.8500 0.3250 0.0980];
% graph.CData(6,:) = [0.8500 0.3250 0.0980];
% graph.CData(8,:) = [0.8500 0.3250 0.0980];
% hold off;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%% DEL) FR1 Device Data Rates [DL/UL] %%%%%%%%%%%%
% hold on;
% xlabel('Number of users') 
% ylabel('Average User Data Rates in FR1 (Mbps)')
% plot(devices, dl_data_rates_FR1_30KHz,'-s');
% plot(devices, dl_data_rates_FR1_60KHz,'-s');
% plot(devices, ul_data_rates_FR1_30KHz,'-o');
% plot(devices, ul_data_rates_FR1_60KHz,'-o');
% legend({'[DL] 30 KHz SCS','[DL] 60 KHz SCS','[UL] 30 KHz SCS','[UL] 60 KHz SCS'},'Location','southwest')
% hold off;

%%%%%%%%%%% DEL) FR2 Device Data Rates [DL/UL] %%%%%%%%%%%%
% hold on;
% xlabel('Number of users') 
% ylabel('Average User Data Rates in FR2 (Mbps)')
% plot(devices, dl_data_rates_FR2_60KHz,'-s');
% plot(devices, dl_data_rates_FR2_120KHz,'-s');
% plot(devices, ul_data_rates_FR2_60KHz,'-o');
% plot(devices, ul_data_rates_FR2_120KHz,'-o');
% legend({'[DL] 60 KHz SCS','[DL] 120 KHz SCS','[UL] 60 KHz SCS','[UL] 120 KHz SCS'},'Location','southwest')
% hold off;

%%%%%%%%%%% DEL) FR1 Connection Percentage [UL] %%%%%%%%%%%%
% hold on;
% xlabel('Number of users') 
% ylabel('UL Macro/Small Connections in FR1 (%)')
% plot(devices, ul_macro_connections_FR1_30KHz,'-s');
% plot(devices, ul_macro_connections_FR1_60KHz,'-s');
% plot(devices, ul_small_connections_FR1_30KHz,'-o');
% plot(devices, ul_small_connections_FR1_60KHz,'-o');
% legend({'Macrocell connections (30 KHz SCS)','Macrocell connections (60 KHz SCS)','Small cell connections (30 KHz SCS)','Small cell connections (60 KHz SCS)'},'Location','southwest')
% hold off;

%%%%%%%%%%% DEL) FR2 Connection Percentage [DL] %%%%%%%%%%%%
% hold on;
% xlabel('Number of users') 
% ylabel('DL Macro/Small Connections in FR2 (%)')
% plot(devices, dl_macro_connections_FR2_60KHz,'-s');
% plot(devices, dl_macro_connections_FR2_120KHz,'-s');
% plot(devices, dl_small_connections_FR2_60KHz,'-o');
% plot(devices, dl_small_connections_FR2_120KHz,'-o');
% legend({'Macrocell connections (60 KHz SCS)','Macrocell connections (120 KHz SCS)','Small cell connections (60 KHz SCS)','Small cell connections (120 KHz SCS)'},'Location','southwest')
% hold off;

%%%%%%%%%%% DEL) FR2 Connection Percentage [UL] %%%%%%%%%%%%
% hold on;
% xlabel('Number of users') 
% ylabel('UL Macro/Small Connections in FR2 (%)')
% plot(devices, ul_macro_connections_FR2_60KHz,'-s');
% plot(devices, ul_macro_connections_FR2_120KHz,'-s');
% plot(devices, ul_small_connections_FR2_60KHz,'-o');
% plot(devices, ul_small_connections_FR2_120KHz,'-o');
% legend({'Macrocell connections (60 KHz SCS)','Macrocell connections (120 KHz SCS)','Small cell connections (60 KHz SCS)','Small cell connections (120 KHz SCS)'},'Location','southwest')
% hold off;

%%%%%%%%%%% DEL) FR1 Connection Percentage [DL/UL] %%%%%%%%%%%%
% hold on;
% xlabel('Number of users') 
% ylabel('Overall Connections in FR1 (%)')
% plot(devices, dl_connections_percent_FR1_30KHz,'-s');
% plot(devices, dl_connections_percent_FR1_60KHz,'-s');
% plot(devices, ul_connections_percent_FR1_30KHz,'-o');
% plot(devices, ul_connections_percent_FR1_60KHz,'-o');
% legend({'[DL] 30 KHz connections','[DL] 60 KHz connections','[UL] 30 KHz connections','[UL] 60 KHz connections'},'Location','southwest')
% hold off;

%%%%%%%%%%% DEL) FR2 Connection Percentage [DL/UL] %%%%%%%%%%%%
% hold on;
% xlabel('Number of users') 
% ylabel('Overall Connections in FR2 (%)')
% plot(devices, dl_connections_percent_FR2_60KHz,'-s');
% plot(devices, dl_connections_percent_FR2_120KHz,'-s');
% plot(devices, ul_connections_percent_FR2_60KHz,'-o');
% plot(devices, ul_connections_percent_FR2_120KHz,'-o');
% legend({'[DL] 60 KHz SCS','[DL] 120 KHz SCS','[UL] 60 KHz SCS','[UL] 10 KHz SCS'},'Location','southwest')
% hold off;

%%%%%%%%%%% DEL) FR1 Connection Percentage [DL] %%%%%%%%%%%%
% hold on;
% xlabel('Number of users') 
% ylabel('DL Macro/Small Connections in FR1 (%)')
% plot(devices, dl_macro_connections_FR1_30KHz,'-s');
% plot(devices, dl_macro_connections_FR1_60KHz,'-s');
% plot(devices, dl_small_connections_FR1_30KHz,'-o');
% plot(devices, dl_small_connections_FR1_60KHz,'-o');
% legend({'Macrocell connections (30 KHz SCS)','Macrocell connections (60 KHz SCS)','Small cell connections (30 KHz SCS)','Small cell connections (60 KHz SCS)'},'Location','southwest')
% hold off;